package com.example.demo.controllers;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Categoria;
import com.example.demo.services.CategoriaService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="/api/categorias")
public class CategoriaController {


	@Autowired()
	private CategoriaService categoriaService;

	@GetMapping
	@ApiOperation(value ="Retorna la lista de categorias",notes="Retorna la lista de todas las categorias")
	private ResponseEntity<List<Categoria>> getAllCategorias() {
		return ResponseEntity.ok(categoriaService.findAll());
	}

	@GetMapping("/{id}")
	@ApiOperation(value ="Retorna la categoria deacuerdo al ID ingresado",notes="Retorna la categoria deacuerdo al ID ingresado")
	private ResponseEntity<Optional<Categoria>> getCuenta(@PathVariable("id") long idCategoria) {

		return ResponseEntity.ok(categoriaService.findById(idCategoria));
	}

	@PostMapping
	@ApiOperation(value ="Agregar categoria",notes="Agregar nueva categoria")
	private ResponseEntity<Categoria> saveCategoria(@RequestBody Categoria categoria) {
		try {
			Categoria categoriaSaved = categoriaService.save(categoria);
			return ResponseEntity.created(new URI("/api/categorias" + categoriaSaved.getId())).body(categoriaSaved);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	@PutMapping("/{id}")
	@ApiOperation(value ="Actualizar categoria",notes="Actualizar registro completo de categoria")
	private ResponseEntity<Categoria> update(@RequestBody Categoria categoria, @PathVariable("id") long idCategoria) {
		try {
			Categoria response = categoriaService.update(idCategoria, categoria);
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	@PatchMapping("/{id}")
	@ApiOperation(value ="actualizar categoria parcial",notes="Actualiza registro de categoria segun valores enviados")
	private ResponseEntity<Categoria> patch(@RequestBody Categoria categoria,
			@PathVariable("id") long idCategoria) {
		try {
			Categoria response = categoriaService.patch(idCategoria, categoria);
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	@DeleteMapping("/{id}")
	@ApiOperation(value ="Eliminar categoria",notes="Elimina registro de categoria deacuerdo a su ID")
	private ResponseEntity<Boolean> delete(@PathVariable("id") long idCategoria) {
		categoriaService.deleteById(idCategoria);
		return ResponseEntity.ok((categoriaService.findById(idCategoria) != null));
	}

}

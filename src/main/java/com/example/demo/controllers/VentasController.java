package com.example.demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Producto;
import com.example.demo.repository.VentasRepository;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/ventas")
public class VentasController {

	@Autowired
	private VentasRepository ventasRepository;
	
	@GetMapping
	@ApiOperation(value ="Retorna las ventas",notes="Retorna la lista de todas las ventas")
	private ResponseEntity<List<Producto>> getAllVentas() {
		return ResponseEntity.ok(ventasRepository.productos());
	}
	
}

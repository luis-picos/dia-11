package com.example.demo.repository;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.model.Producto;

@FeignClient(name="ventas",url="http://localhost:8080/api/productos")
public interface VentasRepository {

	@RequestMapping(method = RequestMethod.GET)
	List<Producto> productos();
}
